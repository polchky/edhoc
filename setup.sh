#!/bin/bash
# First, makes sure that the OSCORE dependencies are cloned
# Then, the missing package pyparsing is installed. If the PIO virtual
# environment is found, it is installed only there, otherwise globally

cd lib/oscore/oscore-implementation/tests/native/ && make libs
if [ -d "$HOME/.platformio/penv" ]; then
    cd ~/.platformio/penv && source bin/activate
fi
pip install pyparsing
