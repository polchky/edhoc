#!/bin/bash
# Disables ASAN, enables stack size information generation, then (re)compiles

cd ../

sed -i -e '/^.*FLAGS += -fsanitize=undefined -fsanitize=address$/s/^/#/' lib/oscore/oscore-implementation/tests/native/Makefile
sed -i -e '/CFLAGS += /s/$/ -fdump-rtl-dfinish -fstack-usage/' lib/oscore/Makefile.oscore

sed -i -e '/-fdump-rtl-dfinish -fstack-usage/s/#//' CMakeLists.txt
sed -i -e '/-fsanitize=undefined -fsanitize=address/s/^/#/' CMakeLists.txt

mkdir -p build && cd build || exit
cmake ..
make clean && make oscoreclean
make edhoc-demo
