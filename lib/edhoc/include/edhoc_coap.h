#ifndef EDHOC_COAP_IMPLEMENTATION_EDHOC_H
#define EDHOC_COAP_IMPLEMENTATION_EDHOC_H

/** @file */

/** @addtogroup edhoc_api EDHOC API
 *
 * @brief API providing the EDHOC protocol
 *
 * The methods marked with `coap` can be used to work on CoAP objects of the
 * type defined in the CoAP backend of the OSCORE library. They offer the
 * advantage of setting the request path and setting the correct correlation
 * value even if an empty message 0 is used to start the protocol.
 *
 * @{
 */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <oscore_native/msg_type.h>
#include <crypto_proxy.h>
#include <edhoc.h>

#define COAP_CODE_POST      0x02 /**< CoAP message code for POST */
#define COAP_CODE_CHANGED   0x44 /**< CoAP response code for CHANGED */

/** @brief Find the EDHOC context associated to a CoAP request
 *
 * This method cannot find the context for CoAP responses, since the connection
 * identifier will not be part of the message. Instead, responses have to be
 * matched to requests with the CoAP token.
 *
 * If the message is an EDHOC message 1, a new context will have to be created.
 *
 * @param[in] ctx_list Context list to search through
 * @param[in] ctx_list_len Length of @p ctx_list
 * @param[in] msg The message to analyze
 * @param[out] ctx The found EDHOC context (if any)
 */
edhoc_error_t edhoc_find_context_coap(
        edhoc_context_t **ctx_list,
        size_t ctx_list_len,
        oscore_msg_native_t msg,
        edhoc_context_t **ctx
);

/** @brief Produce an empty CoAP message to initiate EDHOC
 *
 * Sets the message code and path options. The actual EDHOC message 1 will be
 * the response to this request.
 *
 * @param[inout] msg The message to complete
 */
edhoc_error_t edhoc_start_protocol_coap_empty(oscore_msg_native_t msg);

/** @brief Produce an initial EDHOC message wrapped in CoAP
 *
 * Analogous to @ref edhoc_write_message_1; but sets the correct message code
 * and if EDHOC_COAP_SET_OPTS is defined, also the path as well as the content
 * format options.
 *
 * This method creates a CoAP request. Use @ref edhoc_handle_coap_message; to
 * produce a response to a CoAP message 0.
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[inout] msg The message to complete
 */
edhoc_error_t edhoc_start_protocol_coap(
        edhoc_context_t *ctx,
        oscore_msg_native_t msg
);

/** @brief Treat an EDHOC message wrapped in CoAP and react in an appropriate way
 *
 * This is analogous to @ref edhoc_handle_message; but works with messages
 * wrapped in CoAP.
 *
 * If the input message was a CoAP request then the output will be a CoAP
 * response and vice versa.
 *
 * @note The path as well as the content format options are only set if
 * EDHOC_COAP_SET_OPTS is defined. Otherwise this has to be done before calling
 * this method.
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param msg_in Input message to react to
 * @param msg_out Output message where the response is written to
 */
edhoc_error_t edhoc_handle_coap_message(
        edhoc_context_t *ctx,
        oscore_msg_native_t msg_in,
        oscore_msg_native_t msg_out
);

/** @brief Get the actual error description (text) out of an EDHOC error message
 *
 * @param[in] err_msg The error message
 * @param[out] reason Output, where the description pointer will be placed in
 * @param[out] reason_len Length of the description text
 */
edhoc_error_t edhoc_get_coap_err_msg_reason(
        oscore_msg_native_t err_msg,
        const char **reason,
        size_t *reason_len
);

/** @} */

#endif //EDHOC_COAP_IMPLEMENTATION_EDHOC_H
