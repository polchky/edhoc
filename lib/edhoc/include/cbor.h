#ifndef EDHOC_IMPLEMENTATION_CBOR_H
#define EDHOC_IMPLEMENTATION_CBOR_H

/** @file */

/** @ingroup oscore_extensions
 *  @addtogroup oscore_cbor Internal CBOR API
 *
 *  @brief API to encode/decode data in CBOR format
 *
 *  These functions help to deal with CBOR, but are not part of the public
 *  OSCORE API.
 *  See: https://marcovr.gitlab.io/oscore-implementation/group__oscore__cbor.html
 * 
 *  Reusing OSCORE-CBOR functionality to avoid code duplication and having two
 *  identically named translation units (cbor.o), which causes problems for
 *  calculating the estimated stack size usage.
 *
 *  @{
 */

#include <oscore/internal/cbor.h>

#define edhoc_cbor_major oscore_cbor_major
#define edhoc_cbor_intsize oscore_cbor_intsize
#define edhoc_cbor_intencode oscore_cbor_intencode
#define edhoc_cbor_bstrsize oscore_cbor_bstrsize
#define edhoc_cbor_bstrencode oscore_cbor_bstrencode
#define edhoc_cbor_intdecode oscore_cbor_intdecode

/** @} */

#endif //EDHOC_IMPLEMENTATION_CBOR_H
