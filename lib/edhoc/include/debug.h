#ifndef EDHOC_IMPLEMENTATION_DEBUG_H
#define EDHOC_IMPLEMENTATION_DEBUG_H

/** @file
 * 
 *  Collection of functions to help debug CBOR messages.
 * 
 *  @{
 */

#include <stdint.h>
#include <stdio.h>

/** Print buffer as hexstring - optionally with title */
void dump(char *title, const uint8_t *buf, size_t len);

/** Print buffer as text if possible, otherwise as hexstring - optionally with title */
void dumpdiag(char *title, const uint8_t* buf, size_t len);

/** Interpret buffer as cbor and print its structure - optionally with title */
void dumpcbor(char *title, const uint8_t *buf, size_t len);

/** Interpret buffer as cbor and print its structure - optionally with title.
 * Interprets all bstr attributes as cbor too (non-recursive) */
void dumpcborx(char *title, const uint8_t *buf, size_t len);


/** @} */

#endif //EDHOC_IMPLEMENTATION_DEBUG_H
