#ifndef EDHOC_SHARED_IMPLEMENTATION_EDHOC_H
#define EDHOC_SHARED_IMPLEMENTATION_EDHOC_H

/** @file */

/** @addtogroup edhoc_api EDHOC API
 *
 * @brief API providing the EDHOC protocol
 *
 * @{
 */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <oscore_native/msg_type.h>
#include <crypto_proxy.h>

#define EDHOC_MSG_CORR_COAP      1 /**< EDHOC correlation number for CoAP */
#define EDHOC_MSG_CORR_COAP_RESP 2 /**< EDHOC correlation number for CoAP with msg0 */

#define EDHOC_AEAD_KEYSIZE  16 /**< EDHOC AEAD key size (AES-CCM) */
#define EDHOC_KEYSIZE       32 /**< EDHOC ED25519 key size */
#define EDHOC_THSIZE        34 /**< EDHOC transcript hash bstr size */
#define EDHOC_HASHSIZE      32 /**< EDHOC hash size (SHA-256) */
#define EDHOC_MAX_CIDSIZE   8 /**< EDHOC maximum connection ID size */


/** @brief Enumeration of all possible return values of EDHOC methods
 * It can either represent a success (@ref EDHOC_OK) or failure.
 */
typedef enum edhoc_error_t {
    EDHOC_OK, /**< Success / no error */
    EDHOC_ERROR_INVALID_MSG, /**< Message is either malformed or not supported */
    EDHOC_ERROR_IS_ERROR_MSG, /**< The message is an EDHOC error message */
    EDHOC_ERROR_BUFFER_TOO_SHORT, /**< Some buffer was too small, probably the message buffer */
    EDHOC_ERROR_ERR_MSG_BUF_TOO_SHORT, /**< Tried to write an error message, but the message buffer was too small */
    EDHOC_ERROR_INVALID_KEYSIZE, /**< The provided key is of unsupported size */
    EDHOC_ERROR_INVALID_COORD, /**< The provided coordinate is invalid */
    EDHOC_ERROR_UNSUPPORTED_METHOD, /**< The EDHOC method @ref edhoc_method_t is not supported */
    EDHOC_ERROR_UNSUPPORTED_SUITE, /**< The ciphersuite is not supported */
    EDHOC_ERROR_NATIVE_COAP_ERR, /**< The CoAP library returned an error */
    EDHOC_ERROR_CRYPTO_FAILED, /**< Some cryptographic operation failed */
    EDHOC_ERROR_KEY_NOT_FOUND, /**< The specified public key could not be found */
    EDHOC_ERROR_PROTOCOL_UNFINISHED, /**< The EDHOC protocol needs to be finished before attempting this operation */
    EDHOC_ERROR_PROTOCOL_FINISHED, /**< The EDHOC protocol was already finished, duplicate message? */
    EDHOC_ERROR_CREATE_NEW_CTX, /**< A new EDHOC context should be created to handle this message */
    EDHOC_ERROR_CTX_NOT_FOUND, /**< No EDHOC context found for this message, did you delete the context too early? */
    EDHOC_ERROR_NO_CID, /**< No connection ID found in this message, you are responsible to retrieve the EDHOC context it yourself */
    EDHOC_ERROR_CID_TOO_LONG, /**< The provided connection ID is too long */
    EDHOC_ERROR_OTHER /**< Some unknown error occurred */
} edhoc_error_t;

/** @brief EDHOC method type
 * EDHOC supports both asymmetric and symmetric methods.
 */
typedef enum edhoc_method_t {
    EDHOC_METHOD_ASYMMETRIC = 0, /**< Public/private keys are used */
    EDHOC_METHOD_SYMMETRIC =  1, /**< A shared key is used */
    EDHOC_METHOD_ERROR =      2  /**< Invalid method */
} edhoc_method_t;


/** @brief Structure representing static keys as well as the public key identifier
 * The fields are pointers since the values are provided from externally and
 * might be used for multiple purposes.
 */
typedef struct edhoc_auth_keypair_t {
    const uint8_t *public;
    const uint8_t *private;
    const uint8_t *kid;
    size_t kid_len;
} edhoc_auth_keypair_t;

/** @brief Ephemeral keypair structure
 * Here, the fields aren't pointers but buffers since the keys are generated on
 * initialization and only used together with the EDHOC context.
 */
typedef struct edhoc_ecdh_keypair_t {
    uint8_t coord_x[EDHOC_KEYSIZE];
    uint8_t private[EDHOC_KEYSIZE];
} edhoc_ecdh_keypair_t;

/** @brief Structure used to hold information about the peer
 * Again, the fields are buffers, where the (ephemeral) information is stored
 * as it is received.
 */
typedef struct edhoc_peer_context_t {
    edhoc_auth_keypair_t *auth_keypair;
    uint8_t ecdh_coord_x[EDHOC_KEYSIZE];
    uint8_t conn_id[EDHOC_MAX_CIDSIZE];
    size_t conn_id_len;
} edhoc_peer_context_t;

/** @brief Cipher suite structure with predefined suite number and COSE algorithm list */
typedef struct edhoc_cipher_suite_t {
    int8_t number;
    int32_t algs[4];
} edhoc_cipher_suite_t;

/** @brief EDHOC context which holds the protocol state and all required information
 * The transcript_hash_state is necessary to store partial hashes, otherwise
 * messages need to be kept in memory even after being sent.
 */
typedef struct edhoc_context_t {
    uint8_t type;
    edhoc_cipher_suite_t suite;
    edhoc_auth_keypair_t *auth_keypair;
    edhoc_ecdh_keypair_t ecdh_keypair;
    edhoc_peer_context_t peer;
    uint8_t conn_id[EDHOC_MAX_CIDSIZE];
    size_t conn_id_len;
    uint8_t msg_num;
    /*
     * Problematic if defined as pointer to struct!
     * Would have to be dynamically allocated in that case.
     */
    edhoc_crypto_hashstate_t transcript_hash_state;
    bool party_u;
    edhoc_auth_keypair_t **peer_keys;
    size_t peer_keys_len;
} edhoc_context_t;

/** Simple enumeration of EDHOC message types */
typedef enum edhoc_msg_kind_t {
    EDHOC_MSG_ERROR,
    EDHOC_MSG_1,
    EDHOC_MSG_2,
    EDHOC_MSG_3,
    EDHOC_MSG_UNKNOWN
} edhoc_msg_kind_t;

/** @privatesection */

/** @brief Read and treat EDHOC message 1
 *
 * Reads all information from the message and starts the transcript hash.
 * Invalid messages are rejected.
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[in] buffer Message buffer to read from
 * @param[in] buffer_len length of @param buffer
 */
edhoc_error_t edhoc_read_message_1(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len
);

/** @brief Read and treat EDHOC message 2
 *
 * Reads all information from the message, starts the transcript hash and then
 * decrypts and treats the ciphertext.
 * Invalid messages are rejected.
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[inout] buffer Message buffer to read from
 * @param[in] buffer_len length of @param buffer
 */
edhoc_error_t edhoc_read_message_2(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len
);

/** @brief Read and treat EDHOC message 3
 *
 * Reads all information from the message, starts the transcript hash and then
 * decrypts and treats the ciphertext.
 * Invalid messages are rejected.
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[inout] buffer Message buffer to read from
 * @param[in] buffer_len length of @param buffer
 */
edhoc_error_t edhoc_read_message_3(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len
);

/** @brief Produce the initial EDHOC message
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[out] buffer Output buffer where the message is written to
 * @param[in] buffer_len Length of @p buffer
 * @param[out] msg_len Length of the final message
 */
edhoc_error_t edhoc_write_message_1(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
);

/** @brief Produce the second EDHOC message
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[out] buffer Output buffer where the message is written to
 * @param[in] buffer_len Length of @param buffer
 * @param[out] msg_len Length of the final message
 */
edhoc_error_t edhoc_write_message_2(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
);

/** @brief Produce the third EDHOC message
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[out] buffer Output buffer where the message is written to
 * @param[in] buffer_len Length of @param buffer
 * @param[out] msg_len Length of the final message
 */
edhoc_error_t edhoc_write_message_3(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
);

/** @brief Produce an EDHOC error message if applicable
 *
 * No message is produced for non-errors or local errors.
 * If the error message cannot be written because the message buffer is too
 * small, a special error value is returned to distinguish it from normal
 * errors: @ref EDHOC_ERROR_ERR_MSG_BUF_TOO_SHORT;
 * Otherwise, the original error value is returned.
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[in] error The error value to produce a message for
 * @param[out] buffer Message buffer where the error message is written to
 * @param[in] buffer_len Length of @param buffer
 * @param[out] msg_len Length of the final error message
 */
edhoc_error_t edhoc_write_message_err(
        edhoc_context_t *ctx,
        edhoc_error_t error,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
);

/** Initialize predefined cipher suite with COSE algorithm numbers */
edhoc_error_t edhoc_init_cipher_suite(int8_t suite_number, edhoc_cipher_suite_t *suite);

/** Determine which kind of EDHOC message (1, 2, 3, ERR) is in the buffer */
edhoc_msg_kind_t edhoc_determine_message_kind(const uint8_t *buffer, size_t buffer_len);

/** Detect whether a message is an EDHOC error message
 *
 * @param[in] buffer The message to check
 * @param[in] buffer_len Length of @param buffer
 * @return true if it is an EDHOC error message, false otherwise
 */
bool edhoc_is_error_msg(const uint8_t *buffer, size_t buffer_len);

/** @brief Start a new transcript hash with 1 or 2 chunks of data
 *
 * Prepares a new transcript hash, where some of the necessary data might only
 * be available at a later point.
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[in] data1 First data chunk
 * @param[in] data1_len Length of @param data1
 * @param[in] data2 Second data chunk, can be NULL
 * @param[in] data2_len Length of @param data2
 */
edhoc_error_t edhoc_transcript_hash_start(
        edhoc_context_t *ctx,
        const uint8_t *data1,
        size_t data1_len,
        const uint8_t *data2,
        size_t data2_len
);

/** @brief Finish and get the current transcript hash
 *
 * The output buffer needs to be large enough for the hash to fit inside.
 *
 * @param[inout] ctx EDHOC context to use and update
 * @param[in] data Last data chunk, can be NULL
 * @param[in] data_len Length of @param data
 * @param[out] transcript_hash Output buffer where the transcript hash is written to
 */
edhoc_error_t edhoc_transcript_hash_final(
        edhoc_context_t *ctx,
        const uint8_t *data,
        size_t data_len,
        uint8_t transcript_hash[EDHOC_THSIZE]
);

/** @} */

#endif //EDHOC_SHARED_IMPLEMENTATION_EDHOC_H
