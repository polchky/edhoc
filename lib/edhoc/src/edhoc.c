#include <edhoc.h>
#include <cbor.h>
#include <oscore_native/msg_type.h>
#include <string.h>

// todo: verify support of individual algorithms in suite after init & msg1
// todo: let user choose (preferred) cipher suite

edhoc_error_t edhoc_init_context(
        edhoc_context_t *ctx,
        edhoc_auth_keypair_t *keypair,
        edhoc_auth_keypair_t **peer_keys,
        size_t peer_keys_len
) {
    static size_t conn_id = 0;
    ctx->type = 4 * EDHOC_METHOD_ASYMMETRIC + EDHOC_MSG_CORR_COAP;
    edhoc_error_t e_err = edhoc_init_cipher_suite(0, &ctx->suite);
    if (e_err != EDHOC_OK) {
        return e_err;
    }
    ctx->auth_keypair = keypair;
    edhoc_crypto_curvealg_t alg;
    edhoc_crypto_curvealg_from_number(&alg, ctx->suite.algs[3]);
    edhoc_cryptoerr_t err = edhoc_crypto_make_keypair(alg, ctx->ecdh_keypair.coord_x, ctx->ecdh_keypair.private);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }
    ctx->msg_num = 0;
    ctx->conn_id_len = edhoc_cbor_intencode(conn_id, ctx->conn_id, CBOR_MAJOR_UINT);
    ctx->peer.conn_id_len = EDHOC_MAX_CIDSIZE;
    ctx->party_u = false;
    ctx->peer_keys = peer_keys;
    ctx->peer_keys_len = peer_keys_len;

    conn_id++;
    // size_t is at least 2 bytes and 65536 connection IDs should be plenty
    if (conn_id == 0xFFFF) {
        conn_id = 0;
    }
    return EDHOC_OK;
}

edhoc_error_t edhoc_find_context(
        edhoc_context_t **ctx_list,
        size_t ctx_list_len,
        const uint8_t *buffer,
        size_t buffer_len,
        edhoc_context_t **ctx
) {
    const uint8_t *buffer_end = buffer + buffer_len;
    const uint8_t *p = buffer;
    size_t output = 0;
    enum edhoc_cbor_major major;
    const uint8_t *id = NULL;
    size_t id_len = 0;

    edhoc_msg_kind_t kind = edhoc_determine_message_kind(buffer, buffer_len);
    switch (kind) {
        case EDHOC_MSG_ERROR:
            // CID is first (if exists), followed by a TSTR
            p += edhoc_cbor_intdecode(p, &output, &major);
            if (major == CBOR_MAJOR_BSTR) {
                id = p;
                id_len = output;
            }
            break;
        case EDHOC_MSG_1:
            // No CID available yet
            return EDHOC_ERROR_CREATE_NEW_CTX;
        case EDHOC_MSG_2:
            // CID is first (if exists), followed by 3 BSTR
            for (int i = 0; i < 3; ++i) {
                p += edhoc_cbor_intdecode(p, &output, &major);
                p += output;
            }
            if (p < buffer_end) {
                p = buffer;
                p += edhoc_cbor_intdecode(p, &output, &major);
                id = p;
                id_len = output;
            }
            break;
        case EDHOC_MSG_3:
            // CID is first (if exists), followed by 1 BSTR
            p += edhoc_cbor_intdecode(p, &output, &major);
            if (p + output < buffer_end) {
                id = p;
                id_len = output;
            }
            break;
        default:
            return EDHOC_ERROR_INVALID_MSG;
    }

    if (id == NULL) {
        return EDHOC_ERROR_NO_CID;
    }
    if (id_len > EDHOC_MAX_CIDSIZE) {
        return EDHOC_ERROR_CID_TOO_LONG;
    }

    for (size_t i = 0; i < ctx_list_len; ++i) {
        if (ctx_list[i] == NULL) continue;
        if (id_len == ctx_list[i]->conn_id_len &&
            memcmp(id, ctx_list[i]->conn_id, id_len) == 0) {
            *ctx = ctx_list[i];
            return EDHOC_OK;
        }
    }
    return EDHOC_ERROR_CTX_NOT_FOUND;
}

edhoc_error_t edhoc_start_protocol(
        edhoc_context_t *ctx,
        uint8_t *buffer,
        size_t buffer_len,
        size_t *msg_len
) {
    return edhoc_write_message_1(ctx, buffer, buffer_len, msg_len);
}

edhoc_error_t edhoc_handle_message(
        edhoc_context_t *ctx,
        uint8_t *buf_in,
        size_t buf_in_len,
        uint8_t *buf_out,
        size_t buf_out_len,
        size_t *msg_out_len
) {
    if (edhoc_is_error_msg(buf_in, buf_in_len)) {
        return EDHOC_ERROR_IS_ERROR_MSG;
    }

    edhoc_error_t err;
    if (ctx->msg_num == 0) {
        err = edhoc_read_message_1(ctx, buf_in, buf_in_len);
        if (err == EDHOC_OK) {
            err = edhoc_write_message_2(ctx, buf_out, buf_out_len, msg_out_len);
        }
    } else if (ctx->msg_num == 1) {
        err = edhoc_read_message_2(ctx, buf_in, buf_in_len);
        if (err == EDHOC_OK) {
            err = edhoc_write_message_3(ctx, buf_out, buf_out_len, msg_out_len);
        }
    } else if (ctx->msg_num == 2) {
        err = edhoc_read_message_3(ctx, buf_in, buf_in_len);
    } else {
        return EDHOC_ERROR_PROTOCOL_FINISHED;
    }

    // Returns the error if it does not need to be sent
    return edhoc_write_message_err(ctx, err, buf_out, buf_out_len, msg_out_len);
}

bool edhoc_error_message_produced(edhoc_error_t error) {
    switch (error) {
        case EDHOC_OK:
        case EDHOC_ERROR_IS_ERROR_MSG:
            // No need to send an error message.
            // Either no error at all or an error message was received.
            return false;
        default:
            return true;
    }
}

edhoc_error_t edhoc_get_err_msg_reason(
        const uint8_t *buffer,
        size_t buffer_len,
        const char **reason,
        size_t *reason_len
) {
    const uint8_t *buffer_end = buffer + buffer_len;
    size_t output = 0;
    enum edhoc_cbor_major major;

    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    if (major == CBOR_MAJOR_TSTR && buffer + output <= buffer_end) {
        *reason = (const char *)buffer;
        *reason_len = output;
        return EDHOC_OK;
    } else if (major != CBOR_MAJOR_BSTR || buffer + output > buffer_end) {
        return EDHOC_ERROR_INVALID_MSG;
    }
    buffer += output;
    buffer += edhoc_cbor_intdecode(buffer, &output, &major);
    if (major == CBOR_MAJOR_TSTR && buffer + output <= buffer_end) {
        *reason = (const char *)buffer;
        *reason_len = output;
        return EDHOC_OK;
    } else {
        return EDHOC_ERROR_INVALID_MSG;
    }
}

bool edhoc_protocol_finished(edhoc_context_t *ctx) {
    return ctx->msg_num == 3;
}

edhoc_error_t edhoc_exporter(
        edhoc_context_t *ctx,
        uint8_t *label,
        size_t label_len,
        uint8_t *output,
        size_t output_len
) {
    if (!edhoc_protocol_finished(ctx)) {
        return EDHOC_ERROR_PROTOCOL_UNFINISHED;
    }

    uint8_t secret[EDHOC_KEYSIZE];
    edhoc_crypto_curvealg_t curvealg;
    edhoc_crypto_curvealg_from_number(&curvealg, ctx->suite.algs[3]);
    edhoc_cryptoerr_t err;
    err = edhoc_crypto_shared_secret(curvealg, secret, ctx->ecdh_keypair.private, ctx->peer.ecdh_coord_x);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_INVALID_COORD;
    }

    edhoc_crypto_hashstate_t hash_cpy = ctx->transcript_hash_state;
    uint8_t transcript_hash[EDHOC_THSIZE];
    edhoc_error_t e_err;
    e_err = edhoc_transcript_hash_final(ctx, NULL, 0, transcript_hash);
    if (e_err != EDHOC_OK) {
        return e_err;
    }
    // Restore hash state - Problematic if edhoc_crypto_hashstate_t is a pointer
    ctx->transcript_hash_state = hash_cpy;

    uint8_t info_len = 11 + edhoc_cbor_bstrsize(label_len) + edhoc_cbor_intsize(output_len * 8) + EDHOC_THSIZE;
    uint8_t info[info_len];
    uint8_t *p = info;
    *p = 0x84;
    p++;
    p += edhoc_cbor_intencode(label_len, p, CBOR_MAJOR_TSTR);
    memcpy(p, label, label_len);
    p += label_len;
    memcpy(p, "\x83\xf6\xf6\xf6\x83\xf6\xf6\xf6\x83", 9);
    p += 9;
    p += edhoc_cbor_intencode(output_len * 8, p, CBOR_MAJOR_UINT);
    *p = 0x40;
    p++;
    memcpy(p, transcript_hash, EDHOC_THSIZE);

    edhoc_crypto_hkdfalg_t alg;
    edhoc_crypto_hkdf_from_number(&alg, ctx->suite.algs[1]);
    err = edhoc_crypto_hkdf_derive(
            alg,
            (uint8_t *)"",
            0,
            secret,
            EDHOC_KEYSIZE,
            info,
            info_len,
            output,
            output_len
    );
    return edhoc_cryptoerr_is_error(err) ? EDHOC_ERROR_CRYPTO_FAILED : EDHOC_OK;
}

// Could use edhoc_exporter for that, but then the hash & secret would be computed twice
edhoc_error_t edhoc_derive_oscore_params(
        edhoc_context_t *ctx,
        struct oscore_master_context *master_ctx
) {
    if (!edhoc_protocol_finished(ctx)) {
        return EDHOC_ERROR_PROTOCOL_UNFINISHED;
    }

    uint8_t secret[EDHOC_KEYSIZE];
    edhoc_crypto_curvealg_t curvealg;
    edhoc_crypto_curvealg_from_number(&curvealg, ctx->suite.algs[3]);
    edhoc_cryptoerr_t err;
    err = edhoc_crypto_shared_secret(curvealg, secret, ctx->ecdh_keypair.private, ctx->peer.ecdh_coord_x);
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_INVALID_COORD;
    }

    edhoc_crypto_hashstate_t hash_cpy = ctx->transcript_hash_state;
    uint8_t transcript_hash[EDHOC_THSIZE];
    edhoc_error_t e_err;
    e_err = edhoc_transcript_hash_final(ctx, NULL, 0, transcript_hash);
    if (e_err != EDHOC_OK) {
        return e_err;
    }
    // Restore hash state - Problematic if edhoc_crypto_hashstate_t is a pointer
    ctx->transcript_hash_state = hash_cpy;

    uint8_t info_key_len = 32 + edhoc_cbor_intsize(master_ctx->master_secret_len * 8) + EDHOC_THSIZE;
    uint8_t info_salt_len = 30 + edhoc_cbor_intsize(master_ctx->master_salt_len * 8) + EDHOC_THSIZE;
    uint8_t info_key[info_key_len];
    uint8_t *info_salt = info_key + info_key_len - info_salt_len;
    uint8_t *p = info_key;
    memcpy(p, "\x84\x74OSCORE Master Secret\x83\xf6\xf6\xf6\x83\xf6\xf6\xf6\x83", 31);
    p += 31;
    p += edhoc_cbor_intencode(master_ctx->master_secret_len * 8, p, CBOR_MAJOR_UINT);
    *p = 0x40;
    p++;
    memcpy(p, transcript_hash, EDHOC_THSIZE);

    edhoc_crypto_hkdfalg_t alg;
    edhoc_crypto_hkdf_from_number(&alg, ctx->suite.algs[1]);
    err = edhoc_crypto_hkdf_derive(
            alg,
            (uint8_t *)"",
            0,
            secret,
            EDHOC_KEYSIZE,
            info_key,
            info_key_len,
            master_ctx->master_secret,
            master_ctx->master_secret_len
    );
    if (edhoc_cryptoerr_is_error(err)) {
        return EDHOC_ERROR_CRYPTO_FAILED;
    }
    p = info_salt;
    memcpy(p, "\x84\x72OSCORE Master Salt\x83\xf6\xf6\xf6\x83\xf6\xf6\xf6\x83", 29);
    p += 29;
    p += edhoc_cbor_intencode(master_ctx->master_salt_len * 8, p, CBOR_MAJOR_UINT);
    *p = 0x40;

    err = edhoc_crypto_hkdf_derive(
            alg,
            (uint8_t *)"",
            0,
            secret,
            EDHOC_KEYSIZE,
            info_salt,
            info_salt_len,
            master_ctx->master_salt,
            master_ctx->master_salt_len
    );

    return edhoc_cryptoerr_is_error(err) ? EDHOC_ERROR_CRYPTO_FAILED : EDHOC_OK;
}
