#include <edhoc_coap.h>
#include <cbor.h>
#include <oscore_native/msg_type.h>
#include <oscore_native/message.h>
#include <string.h>

#define COAP_OPT_PATH       11 /**< CoAP option number for path */
#define COAP_OPT_FORMAT     12 /**< CoAP option number for content format */

// The default path for an EDHOC endpoint over CoAP is .well-known/edhoc
static const uint8_t edhoc_path_1[11] = ".well-known";
static const uint8_t edhoc_path_2[5] = "edhoc";
#ifdef EDHOC_COAP_SET_OPTS
static const uint8_t edhoc_format_id[2] = "\0\0"; // fixme: ID is not defined yet
#endif

/** Add the path options to a CoAP message */
static edhoc_error_t edhoc_add_opt_path(oscore_msg_native_t msg) {
    if (oscore_msgerr_native_is_error(
            oscore_msg_native_append_option(msg, COAP_OPT_PATH, edhoc_path_1, sizeof(edhoc_path_1))
    ) || oscore_msgerr_native_is_error(
            oscore_msg_native_append_option(msg, COAP_OPT_PATH, edhoc_path_2, sizeof(edhoc_path_2))
    )) {
        return EDHOC_ERROR_NATIVE_COAP_ERR;
    }
    return EDHOC_OK;
}

#ifdef EDHOC_COAP_SET_OPTS
/** Add the format option to a CoAP message */
static edhoc_error_t edhoc_add_opt_format(oscore_msg_native_t msg) {
    if (oscore_msgerr_native_is_error(
            oscore_msg_native_append_option(msg, COAP_OPT_FORMAT, edhoc_format_id, sizeof(edhoc_format_id))
    )) {
        return EDHOC_ERROR_NATIVE_COAP_ERR;
    }
    return EDHOC_OK;
}
#endif

/** Prepares a CoAP message by setting the code and options appropriately */
static edhoc_error_t edhoc_prepare_coap_message(oscore_msg_native_t msg, bool answer) {
    oscore_msg_native_set_code(msg, answer ? COAP_CODE_CHANGED : COAP_CODE_POST);
#ifdef EDHOC_COAP_SET_OPTS
    edhoc_error_t err;
    if (!answer) {
        err = edhoc_add_opt_path(msg);
        if (err != EDHOC_OK) {
            return err;
        }
    }
    return edhoc_add_opt_format(msg);
#else
    return EDHOC_OK;
#endif
}

/** Finalizes a CoAP message by trimming the payload to the actual length */
static edhoc_error_t edhoc_finalize_coap_message(oscore_msg_native_t msg, size_t payload_len) {
    return oscore_msgerr_native_is_error(oscore_msg_native_trim_payload(msg, payload_len)) ?
           EDHOC_ERROR_NATIVE_COAP_ERR : EDHOC_OK;
}

/** Convenience method to wrap the corresponding EDHOC message in CoAP */
static edhoc_error_t edhoc_write_coap_message(
        edhoc_context_t *ctx,
        oscore_msg_native_t msg,
        uint8_t num,
        bool answer
) {
    uint8_t *payload;
    size_t payload_len;
    edhoc_error_t err;
    err = edhoc_prepare_coap_message(msg, answer);
    if (err != EDHOC_OK) {
        return err;
    }
    oscore_msg_native_map_payload(msg, &payload, &payload_len);
    if (num == 1) {
        // Correlation is different if this is a response (to msg 0) or request
        ctx->type = 4 * EDHOC_METHOD_ASYMMETRIC + (answer ? EDHOC_MSG_CORR_COAP_RESP : EDHOC_MSG_CORR_COAP);
        err = edhoc_write_message_1(ctx, payload, payload_len, &payload_len);
    } else if (num == 2) {
        err = edhoc_write_message_2(ctx, payload, payload_len, &payload_len);
    } else {
        err = edhoc_write_message_3(ctx, payload, payload_len, &payload_len);
    }
    if (err != EDHOC_OK) {
        return err;
    }
    return edhoc_finalize_coap_message(msg, payload_len);
}

/**
 * Convenience method to wrap the resulting EDHOC message in CoAP
 * If the error value does not require an EDHOC error message to be produced,
 * it is returned as is.
 * */
static edhoc_error_t edhoc_write_coap_message_err(
        edhoc_context_t *ctx,
        edhoc_error_t error,
        bool answer,
        oscore_msg_native_t msg
) {
    uint8_t *payload;
    size_t payload_len;
    edhoc_error_t err;

    if (!edhoc_error_message_produced(error)) {
        return error;
    }

    edhoc_prepare_coap_message(msg, answer);
    oscore_msg_native_map_payload(msg, &payload, &payload_len);
    err = edhoc_write_message_err(ctx, error, payload, payload_len, &payload_len);
    if (err == EDHOC_ERROR_ERR_MSG_BUF_TOO_SHORT) {
        return err;
    }
    return edhoc_finalize_coap_message(msg, payload_len);
}

edhoc_error_t edhoc_find_context_coap(
        edhoc_context_t **ctx_list,
        size_t ctx_list_len,
        oscore_msg_native_t msg,
        edhoc_context_t **ctx
) {
    uint8_t *payload;
    size_t payload_len;
    oscore_msg_native_map_payload(msg, &payload, &payload_len);
    return edhoc_find_context(ctx_list, ctx_list_len, payload, payload_len, ctx);
}

edhoc_error_t edhoc_start_protocol_coap_empty(oscore_msg_native_t msg) {
    oscore_msg_native_set_code(msg, COAP_CODE_POST);
    return edhoc_add_opt_path(msg);
}

edhoc_error_t edhoc_start_protocol_coap(edhoc_context_t *ctx, oscore_msg_native_t msg) {
    return edhoc_write_coap_message(ctx, msg, 1, false);
}

edhoc_error_t edhoc_handle_coap_message(
        edhoc_context_t *ctx,
        oscore_msg_native_t msg_in,
        oscore_msg_native_t msg_out
) {
    uint8_t *payload_in;
    size_t payload_in_len;
    edhoc_error_t err;
    bool answer = (oscore_msg_native_get_code(msg_in) & 0xe0u) == 0;
    oscore_msg_native_map_payload(msg_in, &payload_in, &payload_in_len);

    if (edhoc_is_error_msg(payload_in, payload_in_len)) {
        return EDHOC_ERROR_IS_ERROR_MSG;
    }

    if (ctx->msg_num == 0 && payload_in_len == 0) {
        return edhoc_write_coap_message(ctx, msg_out, 1, answer);
    }
    if (ctx->msg_num == 0) {
        err = edhoc_read_message_1(ctx, payload_in, payload_in_len);
        if (err == EDHOC_OK) {
            err = edhoc_write_coap_message(ctx, msg_out, 2, answer);
        }
    } else if (ctx->msg_num == 1) {
        err = edhoc_read_message_2(ctx, payload_in, payload_in_len);
        if (err == EDHOC_OK) {
            err = edhoc_write_coap_message(ctx, msg_out, 3, answer);
        }
    } else if (ctx->msg_num == 2) {
        err = edhoc_read_message_3(ctx, payload_in, payload_in_len);
    } else {
        return EDHOC_ERROR_PROTOCOL_FINISHED;
    }

    // Returns the error if it does not need to be sent
    return edhoc_write_coap_message_err(ctx, err, answer, msg_out);
}

edhoc_error_t edhoc_get_coap_err_msg_reason(
        oscore_msg_native_t err_msg,
        const char **reason,
        size_t *reason_len
) {
    uint8_t *payload;
    size_t payload_len;
    oscore_msg_native_map_payload(err_msg, &payload, &payload_len);
    return edhoc_get_err_msg_reason(payload, payload_len, reason, reason_len);
}
