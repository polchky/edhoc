set(EDHOC_X ${CMAKE_CURRENT_SOURCE_DIR}/../../examples_shared)

idf_component_register(SRCS ../edh_osc_server.c ${EDHOC_X}/examples_shared.c
        INCLUDE_DIRS ${EDHOC_X})

target_compile_options(${COMPONENT_LIB} PRIVATE -DCRYPTO_SODIUM=1)
