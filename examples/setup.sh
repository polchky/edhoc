#!/bin/bash
# Makes sure that the OSCORE dependencies are cloned

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" || exit ; pwd -P )

cd "$parent_path/../lib/oscore/oscore-implementation/tests/native/" || exit
make libs
