#ifndef EDHOC_IMPLEMENTATION_EXAMPLES_SHARED_H
#define EDHOC_IMPLEMENTATION_EXAMPLES_SHARED_H

/** @file
 * 
 *  Shared functions for examples.
 * 
 *  @{
 */

#ifdef ESP_PLATFORM
#include "coap.h"
#include "esp_log.h"

#define log_info ESP_LOGI
#define log_err ESP_LOGE
#else
#include <coap2/coap.h>
#endif
#include <edhoc_coap.h>
#include <oscore/contextpair.h>

#ifndef ESP_PLATFORM
void log_info(const char *tag, const char *fmt, ...);

void log_err(const char *tag, const char *fmt, ...);
#endif

/** @brief Log successful EDHOC run and show common derived key/salt */
void edhoc_success(const char *tag, edhoc_context_t *ctx);

/** @brief Log failed EDHOC run and show the reason for failure */
void edhoc_failed(
        const char *tag,
        edhoc_error_t err,
        coap_pdu_t *msg_in,
        coap_pdu_t *msg_out
);

/** @brief Set up OSCORE context after successfule EDHOC run */
bool init_oscore(edhoc_context_t *ctx, oscore_context_t *secctx);

/** @} */

#endif //EDHOC_IMPLEMENTATION_EXAMPLES_SHARED_H
