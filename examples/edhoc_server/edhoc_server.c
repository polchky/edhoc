// Adapted from the ESP-IDF coap server example
// https://github.com/espressif/esp-idf/blob/master/examples/protocols/coap_server/main/coap_server_example_main.c
#include <examples_shared.h>
#ifdef ESP_PLATFORM
#include <sys/socket.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"
#else
#include <arpa/inet.h>
#endif

#ifdef ESP_PLATFORM
#define EDHOC_ENDPOINT CONFIG_EXAMPLE_ENDPOINT_PATH
#define COAP_LOG_LEVEL CONFIG_COAP_LOG_DEFAULT_LEVEL
#else
#define EDHOC_ENDPOINT ".well-known/edhoc"
#define COAP_LOG_LEVEL LOG_WARNING
#endif

static const uint8_t AUTH_U_PUBLIC[32] = {
        0x42, 0x4C, 0x75, 0x6A, 0xB7, 0x7C, 0xC6, 0xFD, 0xEC, 0xF0, 0xB3,
        0xEC, 0xFC, 0xFF, 0xB7, 0x53, 0x10, 0xC0, 0x15, 0xBF, 0x5C, 0xBA,
        0x2E, 0xC0, 0xA2, 0x36, 0xE6, 0x65, 0x0C, 0x8A, 0xB9, 0xC7
};
static const uint8_t AUTH_V_PRIVATE[32] = {
        0x74, 0x56, 0xB3, 0xA3, 0xE5, 0x8D, 0x8D, 0x26, 0xDD, 0x36, 0xBC,
        0x75, 0xD5, 0x5B, 0x88, 0x63, 0xA8, 0x5D, 0x34, 0x72, 0xF4, 0xA0,
        0x1F, 0x02, 0x24, 0x62, 0x1B, 0x1C, 0xB8, 0x16, 0x6D, 0xA9
};
static const uint8_t AUTH_V_PUBLIC[32] = {
        0x1B, 0x66, 0x1E, 0xE5, 0xD5, 0xEF, 0x16, 0x72, 0xA2, 0xD8, 0x77,
        0xCD, 0x5B, 0xC2, 0x0F, 0x46, 0x30, 0xDC, 0x78, 0xA1, 0x14, 0xDE,
        0x65, 0x9C, 0x7E, 0x50, 0x4D, 0x0F, 0x52, 0x9A, 0x6B, 0xD3
};
static const uint8_t KID_U[1] = {0xA2};
static const uint8_t KID_V[1] = {0xA3};

static edhoc_auth_keypair_t keypair = {
        .kid = KID_V,
        .public = AUTH_V_PUBLIC,
        .private = AUTH_V_PRIVATE,
        .kid_len = sizeof(KID_V)
};
static edhoc_auth_keypair_t peer_key = {
        .kid = KID_U,
        .public = AUTH_U_PUBLIC,
        .kid_len = sizeof(KID_U)
};
static edhoc_auth_keypair_t *peer_keys[1] = {&peer_key};

#define CTX_LIST_LEN 10
static edhoc_context_t *contexts[CTX_LIST_LEN] = {NULL};

const static char *TAG = "EDHOC_server";


static void message_handler(coap_context_t *cctx, coap_resource_t *resource,
                            coap_session_t *session,
                            coap_pdu_t *request, coap_binary_t *token,
                            coap_string_t *query, coap_pdu_t *response) {
    (void)cctx;
    (void)resource;
    (void)session;
    (void)token;
    (void)query;

    response->code = COAP_CODE_CHANGED;

    edhoc_context_t *ctx;
    edhoc_error_t res;
    res = edhoc_find_context_coap(contexts, CTX_LIST_LEN, request, &ctx);
    if (res == EDHOC_ERROR_CTX_NOT_FOUND) {
        log_err(TAG, "EDHOC context not found!");
        return;
    }
    if (res == EDHOC_ERROR_CREATE_NEW_CTX) {
        ctx = calloc(1, sizeof(*ctx));
        if (ctx == NULL) {
            log_err(TAG, "calloc failed");
            abort();
        }
        res = edhoc_init_context(ctx, &keypair, peer_keys, 1);
        assert(res == EDHOC_OK);
        bool list_full = true;
        for (size_t i = 0; i < CTX_LIST_LEN; ++i) {
            if (contexts[i] == NULL) {
                contexts[i] = ctx;
                list_full = false;
                break;
            }
        }
        if (list_full) {
            free(ctx);
            log_err(TAG, "context list full");
            return;
        }
    }
    res = edhoc_handle_coap_message(ctx, request, response);

    bool cleanup = false;
    if (res != EDHOC_OK) {
        edhoc_failed(TAG, res, request, response);
        cleanup = true;
    } else if (edhoc_protocol_finished(ctx)) {
        edhoc_success(TAG, ctx);
        response->code = COAP_CODE_CHANGED;
        cleanup = true;
    }

    if (cleanup) {
        for (size_t i = 0; i < CTX_LIST_LEN; ++i) {
            if (contexts[i] == ctx) {
                contexts[i] = NULL;
            }
        }
        free(ctx);
    }
}

static void coap_example_server(void *p) {
    (void) p;

    coap_context_t *ctx = NULL;
    coap_address_t serv_addr;
    coap_resource_t *resource = NULL;

    coap_set_log_level(COAP_LOG_LEVEL);

    while (1) {
        coap_endpoint_t *ep = NULL;
        unsigned wait_ms;

        /* Prepare the CoAP server socket */
        coap_address_init(&serv_addr);
        serv_addr.addr.sin.sin_family = AF_INET;
        serv_addr.addr.sin.sin_addr.s_addr = INADDR_ANY;
        serv_addr.addr.sin.sin_port = htons(COAP_DEFAULT_PORT);

        ctx = coap_new_context(NULL);
        if (!ctx) {
            log_err(TAG, "coap_new_context() failed");
            continue;
        }
        ep = coap_new_endpoint(ctx, &serv_addr, COAP_PROTO_UDP);
        if (!ep) {
            log_err(TAG, "udp: coap_new_endpoint() failed");
            goto clean_up;
        }
        resource = coap_resource_init(coap_make_str_const(EDHOC_ENDPOINT), 0);
        if (!resource) {
            log_err(TAG, "coap_resource_init() failed");
            goto clean_up;
        }
        coap_register_handler(resource, COAP_REQUEST_POST, message_handler);
        coap_add_resource(ctx, resource);

        log_info(TAG, "server is ready");

        wait_ms = COAP_RESOURCE_CHECK_TIME * 1000;

        while (1) {
            int result = coap_run_once(ctx, wait_ms);
            if (result < 0) {
                break;
            } else if (result && (unsigned)result < wait_ms) {
                /* decrement if there is a result wait time returned */
                wait_ms -= result;
            }
            if (result) {
                /* result must have been >= wait_ms, so reset wait_ms */
                wait_ms = COAP_RESOURCE_CHECK_TIME * 1000;
            }
        }
    }
    clean_up:
    coap_free_context(ctx);
    coap_cleanup();
#ifdef ESP_PLATFORM
    vTaskDelete(NULL);
#endif
}

#ifdef ESP_PLATFORM
void app_main(void)
{
    ESP_ERROR_CHECK( nvs_flash_init() );
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());

    xTaskCreate(coap_example_server, "coap", 8 * 1024, NULL, 5, NULL);
}
#else
int main() {
    coap_example_server(NULL);
    return 0;
}
#endif
