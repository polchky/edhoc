# Examples

There are two pairs of server/client examples:
* **edhoc_server/client**: An example of how the EDHOC protocol can be used
* **edh_osc_server/client**: An extended version of the above with an exchange of
OSCORE messages after a successful EDHOC protocol run.

The examples can run on:
* a standard machine (x86): see the build instructions in the [main README file](https://gitlab.com/marcovr/edhoc/tree/master)
* an ESP32 device: see the README file in the corresponding example directory
