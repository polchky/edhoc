#!/bin/bash

out=performance_report.txt

{
  echo "Estimated stack usage for full EDHOC exchange (without libsodium)"
  (cd stacksize && ./setup.sh > /dev/null && ./stacksize.sh | tr -d '\n')
  echo " bytes"
  echo
  echo "EDHOC performance metrics"
  platformio test -e native -f edhoc-perf | sed -n -r -e '/IGNORE/s/\\n/\n/g;s/^.+IGNORE: //p'
  echo "OSCORE performance metrics"
  platformio test -e native -f oscore-perf | sed -n -r -e '/IGNORE/s/\\n/\n/g;s/^.+IGNORE: //p'
  echo "done"
} > $out
