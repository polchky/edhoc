# EDHOC implementation
An implementation of the Ephemeral Diffie-Hellman Over COSE (EDHOC) protocol.

At the time, the protocol is not fully standardized yet, this implementation
follows the specs of [draft #14](https://tools.ietf.org/html/draft-selander-ace-cose-ecdhe-14).

Currently, only EDHOC method 0 (EDHOC authenticated with asymmetric keys) and
cipher suite 0 (AES-CCM-16-64-128, HMAC 256/256, X25519, EdDSA, Ed25519) are
supported.

## Build instructions
### Prerequisites
If your OS uses APT, just run the following line to install the necessary
dependencies:
```
sudo apt -y install git build-essential pkg-config libsodium-dev cmake
```

**If you want to run the examples, you also need libcoap:**
```
git clone https://github.com/obgm/libcoap.git && cd libcoap
./autogen.sh
./configure --disable-manpages --disable-doxygen --disable-dtls
make && sudo make install
sudo ldconfig
```

### Build with CMake
```
git clone --recurse-submodules https://gitlab.com/marcovr/edhoc && cd edhoc
mkdir build && cd build # optional
cmake .. && make
```
This builds the EDHOC demo and all examples for native (x86).

### Alternatively, build using PlatformIO:
```
git clone --recurse-submodules https://gitlab.com/marcovr/edhoc && cd edhoc
./setup.sh
platformio run
```
This builds the EDHOC demo for both native (x86) and ESP32.

## How to use & demonstration
Check out the [client/server examples](https://gitlab.com/marcovr/edhoc/tree/master/examples)
or have a look at the [documentation](https://marcovr.gitlab.io/edhoc/group__edhoc__api.html)!

The examples can run on:
* a standard machine (x86): see the build instructions above
* an ESP32 device: see the README files in `examples/*`

> :warning: **The EDHOC/OSCORE combined examples currently only work with a hack:**
> Since libcoap doesn't yet accept the OSCORE option (it rejects it as a
> malformed message), we can redefine it to the ETag option number.
> This is obviously totally wrong, but works fine for the example applications.  
> To do so, use this command:
> ```
> sed -i 's/OSCORE_OPT_NUM 9/OSCORE_OPT_NUM 4/' lib/oscore/oscore-implementation/src/include/oscore/helpers.h
> ```
