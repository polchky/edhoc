#include "../testhelper.h"
#include <unity.h>
#include <edhoc.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <oscore/contextpair.h>
#include <oscore/protection.h>
#include <oscore_native/test.h>
#include <oscore/context_impl/primitive.h>

#include <debug.h>

#ifdef ESP_PLATFORM
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_event.h"
#include "nvs_flash.h"
#endif

static const uint8_t ciphertext[13] = {0x61, 0x2f, 0x10, 0x92, 0xf1, 0x77, 0x6f, 0x1c, 0x16, 0x68, 0xb3, 0x82, 0x5e};

int oscore_wrap() {
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},
            .sender_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                           0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
            .sender_sequence_number = 0x14,
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    // Prepare CoAP message
    oscore_msgerr_native_t msgerr;
    oscore_msg_native_t coap_msg = oscore_test_msg_create();
    oscore_msg_native_set_code(coap_msg, 0x01);
    msgerr = oscore_msg_native_append_option(coap_msg, 3, (uint8_t *) "localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(coap_msg, 11, (uint8_t*)"tv1", 3);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_trim_payload(coap_msg, 0);
    assert(!oscore_msgerr_native_is_error(msgerr));

    // Wrap & Protect message
    oscore_msg_native_t oscore_msg = oscore_test_msg_create();
    enum oscore_protect_request_result result;
    result = oscore_wrap_request(coap_msg, oscore_msg, &secctx);
    assert(result == OSCORE_PROTECT_REQUEST_OK);

    uint8_t *payload;
    size_t payload_len = 0;
    oscore_msg_native_map_payload(oscore_msg, &payload, &payload_len);
    assert(payload_len == sizeof(ciphertext));
    //assert(memcmp(payload, ciphertext, sizeof(ciphertext)) == 0);
    if (memcmp(payload, ciphertext, sizeof(ciphertext)) != 0) {
        dump("CIPHER: ", payload, payload_len);
        dump(NULL, ciphertext, sizeof(ciphertext));
    }

    oscore_test_msg_destroy(coap_msg);
    oscore_test_msg_destroy(oscore_msg);

    return 0;
}

int oscore_unwrap() {
    struct oscore_context_primitive primitive = {
            .aeadalg = 10,
            .common_iv = {0x46, 0x22, 0xd4, 0xdd, 0x6d, 0x94, 0x41, 0x68, 0xee, 0xfb, 0x54, 0x98, 0x7c},

            .recipient_key = {0xf0, 0x91, 0x0e, 0xd7, 0x29, 0x5e, 0x6a, 0xd4,
                              0xb5, 0x4f, 0xc7, 0x93, 0x15, 0x43, 0x02, 0xff},
            .sender_sequence_number = 0x14,
    };
    oscore_context_t secctx = {
            .type = OSCORE_CONTEXT_PRIMITIVE,
            .data = (void*)(&primitive),
    };

    oscore_msgerr_native_t msgerr;
    oscore_msg_native_t oscore_msg = oscore_test_msg_create();
    oscore_msg_native_set_code(oscore_msg, 0x02);
    msgerr = oscore_msg_native_append_option(oscore_msg, 3, (uint8_t *) "localhost", 9);
    assert(!oscore_msgerr_native_is_error(msgerr));
    msgerr = oscore_msg_native_append_option(oscore_msg, OSCORE_OPT_NUM, (uint8_t*)"\x09\x14", 2);
    assert(!oscore_msgerr_native_is_error(msgerr));
    uint8_t *payload;
    size_t payload_len = 0;
    msgerr = oscore_msg_native_map_payload(oscore_msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr) && payload_len > sizeof(ciphertext));
    memcpy(payload, ciphertext, sizeof(ciphertext));
    msgerr = oscore_msg_native_trim_payload(oscore_msg, sizeof(ciphertext));
    assert(!oscore_msgerr_native_is_error(msgerr));

    // Unwrap & Unprotect message
    oscore_msg_native_t coap_msg = oscore_test_msg_create();
    enum oscore_unprotect_request_result result;
    result = oscore_unwrap_request(oscore_msg, coap_msg, &secctx);
    assert(result == OSCORE_UNPROTECT_REQUEST_OK);

    msgerr = oscore_msg_native_map_payload(coap_msg, &payload, &payload_len);
    assert(!oscore_msgerr_native_is_error(msgerr));
    assert(payload_len == 0);

    oscore_test_msg_destroy(coap_msg);
    oscore_test_msg_destroy(oscore_msg);

    return 0;
}

void oscore_perf(char *desc, int test()) {
    struct timer_results results;
    int res = run_for_duration(test, 10, &results);
    if (res != 0) {
        TEST_FAIL_MESSAGE("Failed to measure performance");
    }
    char msg[200];
    int offset = sprintf(msg, "%s\n", desc);
    format_results(msg + offset, &results);
    TEST_IGNORE_MESSAGE(msg);
}

void oscore_perf_wrap() {
    oscore_perf("Performance for wrapping (protect)", oscore_wrap);
}

void oscore_perf_unwrap() {
    oscore_perf("Performance for unwrapping (unprotect)", oscore_unwrap);
}

void run_tests() {
    UNITY_BEGIN();
    RUN_TEST(oscore_perf_wrap);
    RUN_TEST(oscore_perf_unwrap);
    UNITY_END();
}

#ifdef ESP_PLATFORM
void app_main(void)
{
    ESP_ERROR_CHECK( nvs_flash_init() );
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    xTaskCreate(run_tests, "edhoc", 8 * 1024, NULL, 5, NULL);
}
#else
int main() {
    run_tests();
}
#endif
