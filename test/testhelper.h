#ifndef EDHOC_IMPLEMENTATION_TESTHELPER_H
#define EDHOC_IMPLEMENTATION_TESTHELPER_H

/** @file
 * 
 *  Collection of functions for measuring test performance.
 * 
 *  @{
 */

/** @brief Struct which holds the number of runs, the total runtime and the average time for each run. */
struct timer_results {
    long runs;
    double total_time;
    double avg_time;
};

/** @brief (Re)set the timer. There is only one global timer. */
void reset_timer();
/** @brief Get the time (in seconds) since the timer was last reset. Does not reset the timer. */
double get_time();

/** @brief Runs a task repeatedly until the specified duration (in seconds) is exceeded. */
int run_for_duration(int task(), double duration, struct timer_results *results);
/** @brief Formats the results into textual form. */
int format_results(char *msg, struct timer_results *results);

/** @} */

#endif //EDHOC_IMPLEMENTATION_TESTHELPER_H
