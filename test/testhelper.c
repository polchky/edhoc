#include <time.h>
#include <stdio.h>
#include "testhelper.h"

clock_t start;

void reset_timer() {
    start = clock();
}

double get_time() {
    clock_t end = clock();
    double cpu_time = ((double) (end - start)) / CLOCKS_PER_SEC;
    return cpu_time;
}

int run_for_duration(int task(), double duration, struct timer_results *results) {
    results->runs = 0;
    results->total_time = 0;
    reset_timer();
    while (results->total_time < duration) {
        int res = task();
        if (res != 0) {
            return res;
        }
        results->total_time = get_time();
        results->runs++;
    }
    results->avg_time = results->total_time / (double) results->runs;
    return 0;
}

int format_results(char *msg, struct timer_results *results) {
    // For some reason, printf with floats crashes the ESP-32, thus the float is split into two integer parts
#ifdef ESP_PLATFORM
    int t_int = (int) results->total_time;
    int t_frac = (int)((results->total_time - t_int) * 100000);
    int a_int = (int) results->avg_time;
    int a_frac = (int)((results->avg_time - a_int) * 100000);
    return snprintf(msg, 200, "Elapsed time: %d.%05d seconds, number of runs: %ld\n"
                              "AVG time per run: %d.%05d seconds\n", t_int, t_frac, results->runs, a_int, a_frac);
#else
    return snprintf(msg, 200, "Elapsed time: %f seconds, number of runs: %ld\n"
                              "AVG time per run: %f seconds\n", results->total_time, results->runs, results->avg_time);
#endif
}
